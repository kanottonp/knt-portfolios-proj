import api from '@/middleware/api.js'


export default {
    addTalent(credentials) {
        return api().post('add-talent', credentials)
    },

    getTalent() {
        return api().get('get-talent')
    },

    deleteTalent(credentials) {
        return api().post('del-talent', credentials)
    },

    addContact(credentials) {
        return api().post('add-contact', credentials)
    },

    getContact() {
        return api().get('get-contact')
    },



}