'use strict'

const mongoose = require('mongoose')

const talent = mongoose.model('talentList')

exports.gets = function(req, res) {
    res.send({
        message: 'Welcome!'
    })
}

exports.get_talent = function(req, res) {
    talent.find()
        .then((doc) => {
            res.json({
                item: doc,
            })
        })
}

exports.add_new_talent = function(req, res) {
    var item = {
            talent: req.body.talent,
            year: req.body.year
        },
        update = { expire: new Date() },
        options = { upsert: false };


    talent.findOneAndUpdate(item, update, options, function(error, result) {
        if (!error) {
            // If the document doesn't exist
            if (!result) {
                // Create it
                result = new talent(item);
            }
            // Save the document
            result.save(function(error) {
                if (!error) {
                    console.log(error)
                } else {
                    throw error;
                }
            });
        }
    });

    // var me = talent.findOne(query)
    // console.log(me)
    //     // var data = new talent(item)
    //     // data.save()

    res.send({
        message: `Hello ${req.body.talent}! WOW!`
    })
}

exports.delete_talent = function(req, res) {
    console.log(req.body.id)
    talent
        .findByIdAndRemove(req.body.id)
        .exec()
        .then(doc => {
            if (!doc) { return res.status(404).end() }
            return res.status(204).end()
        })
}

// app.get('/', (req, res) => {
//     res.send({
//         message: 'Welcome!'
//     })
// })

// app.get('/get-user', (req, res) => {
//     UserReg.find()
//         .then((doc) => {
//             res.json({
//                 item: doc,
//                 message: 'KNT Permitted!'
//             })
//         })
// })

// app.post('/register', (req, res) => {
//     var item = {
//         email: req.body.email,
//         password: req.body.password
//     }

//     var data = new UserReg(item)
//     data.save()

//     res.send({
//         message: `Hello ${req.body.email}! Congratz!`
//     })
// })