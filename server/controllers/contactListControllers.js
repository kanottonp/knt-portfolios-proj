'use strict'

const mongoose = require('mongoose')

const contact = mongoose.model('contactList')

exports.gets = function(req, res) {
    res.send({
        message: 'Welcome!'
    })
}

exports.get_contact = function(req, res) {
    contact.find()
        .then((doc) => {
            res.json({
                item: doc,
            })
        })
}

exports.add_new_contact = function(req, res) {
    var item = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email
        },
        update = { expire: new Date() },
        options = { upsert: false };


    contact.findOneAndUpdate(item, update, options, function(error, result) {
        if (!error) {
            // If the document doesn't exist
            if (!result) {
                // Create it
                result = new contact(item);
            }
            // Save the document
            result.save(function(error) {
                if (!error) {
                    console.log(error)
                } else {
                    throw error;
                }
            });
        }
    });

    // var me = contact.findOne(query)
    // console.log(me)
    //     // var data = new contact(item)
    //     // data.save()

    res.send({
        message: `Update ${req.body.firstname} ${req.body.lastname} @ ${req.body.email}! WOW!`
    })
}

// exports.delete_contact = function(req, res) {
//     console.log(req.body.id)
//     contact
//         .findByIdAndRemove(req.body.id)
//         .exec()
//         .then(doc => {
//             if (!doc) { return res.status(404).end() }
//             return res.status(204).end()
//         })
// }

// app.get('/', (req, res) => {
//     res.send({
//         message: 'Welcome!'
//     })
// })

// app.get('/get-user', (req, res) => {
//     UserReg.find()
//         .then((doc) => {
//             res.json({
//                 item: doc,
//                 message: 'KNT Permitted!'
//             })
//         })
// })

// app.post('/register', (req, res) => {
//     var item = {
//         email: req.body.email,
//         password: req.body.password
//     }

//     var data = new UserReg(item)
//     data.save()

//     res.send({
//         message: `Hello ${req.body.email}! Congratz!`
//     })
// })