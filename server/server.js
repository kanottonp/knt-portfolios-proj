const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

const talent = require('../server/models/talentListModels')
const contact = require('../server/models/contactListModels')

const talentRoute = require('../server/routes/talentListRoutes')
const contactRoute = require('../server/routes/contactListRoutes')

talentRoute(app)
contactRoute(app)

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/kntportdb')

app.listen(process.env.PORT || 8080)

console.log("Hello KNT! Server is on!")