'use strict'

module.exports = function(app) {

    var talentList = require('../controllers/talentListControllers')


    app.route('/').get(talentList.gets)

    app.route('/get-talent').get(talentList.get_talent)

    app.route('/add-talent').post(talentList.add_new_talent)

    app.route('/del-talent').post(talentList.delete_talent)

}