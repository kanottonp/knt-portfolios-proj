'use strict'

module.exports = function(app) {

    var contactList = require('../controllers/contactListControllers')


    app.route('/').get(contactList.gets)

    app.route('/get-contact').get(contactList.get_contact)

    app.route('/add-contact').post(contactList.add_new_contact)

    // app.route('/del-contact').post(contactList.delete_contact)

}