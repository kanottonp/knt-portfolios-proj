'use strict'

const mongoose = require('mongoose')

var Schema = mongoose.Schema

var talentList = new Schema({
    talent: {
        type: String
    },
    year: {
        type: Number
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
}, { collection: 'talentList' })

module.export = mongoose.model('talentList', talentList)