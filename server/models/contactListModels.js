'use strict'

const mongoose = require('mongoose')

var Schema = mongoose.Schema

var contactList = new Schema({
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    email: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
}, { collection: 'contactList' })

module.export = mongoose.model('contactList', contactList)