-- Initial New Table
CREATE TABLE intervals AS
SELECT id, user_id, time_in, time_out, TIMESTAMPDIFF(MINUTE,time_in,time_out) as diff
FROM visit -- สร้างตารางขื่อ intervals เก็บ ความต่างของเวลาที่เข้าใช้


-- 1.คนที่เข้ามาใช้บริการคนนั้นๆ
-- เคยเข้ามาใช้บริการกี่ครั้ง เฉลี่ยครั้งละกี่นาที ครั้งแรกและคร้ังสุดท้ายที่เค้าเข้ามาต่างกันกี่วัน -

SELECT COUNT(id) FROM visit WHERE user_id=1; -- USER_ID = 1
SELECT COUNT(id) FROM visit WHERE user_id=2; -- USER_ID = 2
SELECT COUNT(id) FROM visit WHERE user_id=3; -- USER_ID = 3

SELECT AVG(diff) FROM intervals WHERE user_id=1; -- หาเวลาเฉลี่ยของคนที่ 1
SELECT AVG(diff) FROM intervals WHERE user_id=2; -- หาเวลาเฉลี่ยของคนที่ 2
SELECT AVG(diff) FROM intervals WHERE user_id=3; -- หาเวลาเฉลี่ยของคนที่ 3

-- ยังหาเข้าครั้งแรกกับครั้งสุดท้ายต่างกันกี่วันไม่ได้

-- 2. จากการเข้ามาทั้งหมด
-- เราเคยให้บริการแบบละกี่ครั้ง รวมไปถึงไม่ได้ให้บริการในการเข้ามากี่ครั้ง
SELECT COUNT(id)FROM visit; -- จำนวนการเข้าใช้ทั้งหมด
SELECT COUNT(service_id) FROM visit WHERE service_id IS NOT NULL ; -- จำนวนการให้บริการ

-- จำนวนการเข้าใช้ทั้งหมด - จำนวนการให้บริการ = จำนวนที่ไม่ได้ให้บริการ

SELECT COUNT(id) FROM visit WHERE service_id = 1; -- จำนวนการให้บริการ Support
SELECT COUNT(id) FROM visit WHERE service_id = 2; -- จำนวนการให้บริการ Sales
SELECT COUNT(id) FROM visit WHERE service_id = 5; -- จำนวนการให้บริการ Engineer

--3.คนที่เข้ามาใช้บริการคนนั้นๆ
--เข้ามาใช้บริการอะไรบ้าง ใช้บริการด้านนั้นกี่ครั้ง เฉลี่ยกี่นาที สนใจเฉพาะคนที่ใช้บริการมากกว่า 300 นาที

SELECT COUNT(service_id) FROM intervals WHERE service_id = 1 AND user_id = 1 AND diff > 300; -- USER 1 SERVICE 1
SELECT COUNT(service_id) FROM intervals WHERE service_id = 2 AND user_id = 1 AND diff > 300; -- USER 1 SERVICE 2
SELECT COUNT(service_id) FROM intervals WHERE service_id = 5 AND user_id = 1 AND diff > 300; -- USER 1 SERVICE 5

SELECT COUNT(service_id) FROM intervals WHERE service_id = 1 AND user_id = 2 AND diff > 300; -- USER 2 SERVICE 1
SELECT COUNT(service_id) FROM intervals WHERE service_id = 2 AND user_id = 2 AND diff > 300; -- USER 2 SERVICE 2
SELECT COUNT(service_id) FROM intervals WHERE service_id = 5 AND user_id = 2 AND diff > 300; -- USER 2 SERVICE 5

SELECT COUNT(service_id) FROM intervals WHERE service_id = 1 AND user_id = 3 AND diff > 300; -- USER 3 SERVICE 1
SELECT COUNT(service_id) FROM intervals WHERE service_id = 2 AND user_id = 3 AND diff > 300; -- USER 3 SERVICE 2
SELECT COUNT(service_id) FROM intervals WHERE service_id = 5 AND user_id = 3 AND diff > 300; -- USER 3 SERVICE 5